# Poetry-Test

Repo um das Tool "Poetry" und die GitLab Package Registry für Python Packages zu testen.

## Links

- [Offizielle Dokumentation](https://python-poetry.org/docs/)

## Basic Usage / Cheat Sheet

| Ziel | Befehl |
| - | - |
| Neues Projekt anlegen (Boilerplate) | `poetry new $my-cool-project` |
| Poetry für ein bereits bestehendes Projekt initialisieren | `poetry init` |
| Abhängigkeit hinzufügen | `poetry add $my-dependency` |
| Dev-Abhängigkeit hinzufügen (pytest, linter, formatter...) | `poetry add -D $my-dev-dependency` |
| Virtuelle Umgebung aktivieren | `poetry shell` |
| Virtuelle Umgebung deaktivieren | `exit` |
| Virtuelle Umgebungen immer in Projektordner anlegen | `poetry config virtualenvs.in-project true` |
| Einen einzelnen Befehl in virtueller Umgebung ausführen | `poetry run $my-command` |
| Projekt-Version updaten | `poetry version {patch/minor/major}` |
| Package bauen | `poetry build` |
| Package in einer Registry veröffentlichen | `poetry publish` |
| Poetry-Konfiguration nur für das aktuelle Projekt setzen | `poetry config --local config.key value` |

## `pyproject.toml` vs `poetry.lock`

- [`pyproject.toml`](pyproject.toml) beinhaltet die "Grundabhängigkeiten" und deren Versions-Beschränkungen, z.B. `python = "^3.8"` oder `black = "^22.1.0"`
- [`poetry.lock`](poetry.lock) beinhaltet *alle* installierten Pakete - also die Grundabhängigkeiten samt aller aufgelösten Abhängigkeiten - jeweils mit exakter Versionsbezeichnung.

Und wie hängen diese beiden Files jetzt zusammen?

`poetry install` bzw. `poetry update` lesen die in `pyproject.toml` definierten Abhängigkeiten (samt Constraints) und lösen diese auf die aktuellste unter den gegebenen Condtraints mögliche Kombination von Paketen (inklusive "Unter-Abhängigkeiten") auf und schreiben diese (samt exakter Versionen) in `poetry.lock`.

## Den korrekten Python-Interpreter in Editor / IDE auswählen

Damit die ganzen automagischen Dinge wie Code-Completion, Docstrings etc. funktionieren, muss der Editor / die IDE den richtigen Python-Interpreter (der in der virtuellen Umgebung liegt) kennen.

### VS Code

VS Code erkennt, wenn Poetry eine neue virtuelle Umgebung anlegt und listet diese (mit einer kleinen Verzögerung) zur Auswahl auf, wenn man unten in der Statusleiste auf den aktuell ausgewählten Python-Interpreter klickt (Voraussetzung: Das Python-Plugin muss installiert sein(?)).

### PyCharm

In PyCharm kann man den Interpreter auswählen wie hier beschrieben: [Configure a Poetry environment](https://www.jetbrains.com/help/pycharm/poetry.html).

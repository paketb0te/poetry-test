"""
Test-Package
"""


def greeting(name="world") -> str:
    """hello world!

    Args:
        name (str, optional): Name to greet. Defaults to "world".

    Returns:
        str: The greeting.
    """
    return f"Hello {name}!"


def greeting_short(name="world") -> str:
    """Hi world!

    Args:
        name (str, optional): Name to greet. Defaults to "world".

    Returns:
        str: The greeting.
    """
    return f"Hi {name}!"


def greeting_french(name="world") -> str:
    """Bonjour world!

    Args:
        name (str, optional): Name to greet. Defaults to "world".

    Returns:
        str: The greeting.
    """
    return f"Bonjour {name}!"


def greeting_long(name="world") -> str:
    """A warm welcome, world!

    Args:
        name (str, optional): Name to greet. Defaults to "world".

    Returns:
        str: The greeting.
    """
    return f"A warm welcome, {name}!"

"""Tests
"""

import unittest
import poetry_test.main


class TestGreeting(unittest.TestCase):
    """Verify that greeting() works with strings and integers."""

    def test_greeting_string(self):
        """Verify string input."""
        self.assertEqual(poetry_test.main.greeting("Dude"), "Hello Dude!")

    def test_greeting_int(self):
        """Verify integer input."""
        self.assertEqual(poetry_test.main.greeting(1), "Hello 1!")

    def test_greeting_none(self):
        """Verify None input."""
        self.assertEqual(poetry_test.main.greeting(None), "Hello None!")


class TestGreetingShort(unittest.TestCase):
    """Verify that greeting_short() works with strings and integers."""

    def test_greeting_short_string(self):
        """Verify string input."""
        self.assertEqual(poetry_test.main.greeting_short("Dude"), "Hi Dude!")

    def test_greeting_short_int(self):
        """Verify integer input."""
        self.assertEqual(poetry_test.main.greeting_short(1), "Hi 1!")

    def test_greeting_short_none(self):
        """Verify None input."""
        self.assertEqual(poetry_test.main.greeting_short(None), "Hi None!")


class TestGreetingFrench(unittest.TestCase):
    """Verify that greeting_french() works with strings and integers."""

    def test_greeting_short_string(self):
        """Verify string input."""
        self.assertEqual(poetry_test.main.greeting_french("Dude"), "Bonjour Dude!")

    def test_greeting_short_int(self):
        """Verify integer input."""
        self.assertEqual(poetry_test.main.greeting_french(1), "Bonjour 1!")

    def test_greeting_short_none(self):
        """Verify None input."""
        self.assertEqual(poetry_test.main.greeting_french(None), "Bonjour None!")


class TestGreetingLong(unittest.TestCase):
    """Verify that greeting_long() works with strings and integers."""

    def test_greeting_long_string(self):
        """Verify string input."""
        self.assertEqual(
            poetry_test.main.greeting_long("Dude"), "A warm welcome, Dude!"
        )

    def test_greeting_long_int(self):
        """Verify integer input."""
        self.assertEqual(poetry_test.main.greeting_long(1), "A warm welcome, 1!")

    def test_greeting_long_none(self):
        """Verify None input."""
        self.assertEqual(poetry_test.main.greeting_long(None), "A warm welcome, None!")
